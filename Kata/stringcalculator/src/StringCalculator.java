import java.util.ArrayList;
import java.util.List;

public class StringCalculator {


    public static int Add(String numbers) {
        int result = 0;

        if (!numbers.isEmpty()) {
//             if (numbers.contains(",")) {
//            String[] commaNumbers = numbers.split(",");
//            for (int i = 0; i < commaNumbers.length; i++) {
//                if (commaNumbers[i].contains("\n")) {
//                    String[] newLinesNumbers = commaNumbers[i].split("\n");
//                    for (int j = 0; j < newLinesNumbers.length; j++) {
//                        result += Integer.parseInt(newLinesNumbers[j]);
//                    }
//                } else {
//                    result += Integer.parseInt(commaNumbers[i]);
//                }
//
//            }
          if (numbers.contains("//")) {
            List negativeNumbers = new ArrayList();
            numbers = numbers.replace("//", "");
            String[] delimiterNumbers = numbers.split(String.valueOf(numbers.charAt(0)));
            for (int i = 1; i < delimiterNumbers.length; i++) {
                if (delimiterNumbers[i].contains("\n")) {
                    String[] newLinesNumbers = delimiterNumbers[i].split("\n");
                    for (int j = 0; j < newLinesNumbers.length; j++) {
                        //si le \n est au debut de la sous chaine
                        if (newLinesNumbers[j].isEmpty()) {
                            continue;
                        }if (Integer.parseInt(newLinesNumbers[j]) < 0)
                            negativeNumbers.add(newLinesNumbers[j]);
                           result += Integer.parseInt(newLinesNumbers[j]);
                    }
                } else {
                    if (Integer.parseInt(delimiterNumbers[i]) < 0)
                        negativeNumbers.add(delimiterNumbers[i]);
                    result += Integer.parseInt(delimiterNumbers[i]);
                }
            }
            if (negativeNumbers.size() > 0)
                throw new RuntimeException("Negatives not allowed:" + negativeNumbers.toString());
        } else {

            result = Integer.parseInt(String.valueOf(numbers));
            if (result < 0) {
                throw new RuntimeException("Negatives not allowed:[" + String.valueOf(numbers) + "]");
            }
          }
        }

        return result;
    }


}
