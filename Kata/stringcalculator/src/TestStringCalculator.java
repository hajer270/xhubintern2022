import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TestStringCalculator {
    @Test
    public void
    inputIsEmpty() {
        assertEquals(0, StringCalculator.Add(""));
    }

    @Test
    public void inputIs0neNumber() {
        assertEquals(1, StringCalculator.Add("1"));
    }

    @Test
    public void inputIsTwoNumber() {
        assertEquals(13, StringCalculator.Add("//,12,1"));
    }
    @Test
    public void inputIsTreeNumber() {

        assertEquals(14, StringCalculator.Add("//,12,1,1"));
    }
    @Test
    public void inputIsUnknownAmountNumber() {

        assertEquals(577, StringCalculator.Add("//,12,1,1,563"));
    }
    @Test
    public void inputHandleNewLines() {
        assertEquals(6, StringCalculator.Add("//,1\n2,3"));
    }

    @Test
    public void inputDelimitersLines() {
        assertEquals(16, StringCalculator.Add("//@\n1@2@12\n1"));
    }
     @Test(expected = RuntimeException.class)
    public void  inputNegativesNumbers() {
        StringCalculator.Add("-1");
    }
    @Test
    public void  inputNegativesNumbersPassed() {
        RuntimeException exception = null;
        try {
            StringCalculator.Add("-1");
        } catch (RuntimeException e) {
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertEquals("Negatives not allowed:[-1]", exception.getMessage());
    }
}

